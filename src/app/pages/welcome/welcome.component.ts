import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/shared/service/profile.service';
import { Profile, Test2 } from '../../shared/model/Fruits.model';
import * as moment from 'moment/moment.js';
import { LayoutComponent } from '../../core/layout/layout.component';
import * as Highcharts from 'highcharts';
interface ItemData {
  id: string;
  name: string;
  age: string;
  address: string;
}

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
})
export class WelcomeComponent implements OnInit {
  HighchartsPupan: typeof Highcharts = Highcharts;

  dataAge: Array<any>;

  chartOptions: Highcharts.Options = {
    chart: {
      plotShadow: false,
      type: 'pie',
    },
    title: {
      text: '',
    },
    
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
    },
    accessibility: {
      point: {
        valueSuffix: '%',
      },
    },
    legend: {

      
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      itemMarginTop: 10,
      itemMarginBottom: 10,
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
          filter: {
            property: 'percentage',
            operator: '>',
            value: 4,
          },
        },
        showInLegend: true,
      },
    },
    colors: [
      '#EC7063',
      '#F5B041',
      '#85C1E9',
      '#BB8FCE',
      '#58D68D',
      '#E67E22 ',
      '#F1948A',
      '#D7BDE2 ',
      '#F7DC6F ',
      '#A3E4D7 ',
      '#F0B27A ',
    ],
    series: [],
  };

  ischeck: boolean;
  isVisible = false;
  startTime: any;
  checked = false;
  endTime: any;
  isChecked: boolean;
  checkDate: any;
  isDisabled = true;
  minute: any;
  switchValue = false;
  dateNow: any;

  checkPixel: any;

  i = 0;
  editId: string | null = null;
  listOfData: ItemData[] = [];

  constructor(private ServiceProfie: ProfileService) {
    this.ischeck = true;
    this.isChecked = true;

    this.chartOptions.series = [];

    this.dataAge = [
      {
        range: '<10',
        value: 10,
      },
      {
        range: '10-19',
        value: 10,
      },
      {
        range: '20-29',
        value: 10,
      },
      {
        range: '30-39',
        value: 10,
      },
      {
        range: '40-49',
        value: 10,
      },
      {
        range: '50-60',
        value: 10,
      },
      {
        range: '>60',
        value: 10,
      },
    ];
  }

  ngOnInit() {
    console.log();

    var options = [] as Array<any>;

    this.dataAge.forEach((val, index) => {
      options.push({
        name: val.range,
        y: val.value,
      });
    }); //? api call

    console.log(options);

    this.chartOptions.series = [
      {
        name: 'Asset',
        type: 'pie',
        colorByPoint: true,
        data: options,
      },
    ]; //? chart

    this.addRow();
    this.addRow();

    this.checkPixel = this.ServiceProfie.pixel;
  }

  highcharts = (): void => {};

  startEdit(id: string): void {
    this.editId = id;
  }

  stopEdit(): void {
    this.editId = null;
  }

  addRow(): void {
    this.listOfData = [
      ...this.listOfData,
      {
        id: `${this.i}`,
        name: `Edward King ${this.i}`,
        age: '32',
        address: `London, Park Lane no. ${this.i}`,
      },
    ];
    this.i++;
  }

  deleteRow(id: string): void {
    console.log(id);
    this.listOfData = this.listOfData.filter((d) => d.id !== id);
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }
  getDisabledHours1 = () => {
    var hours = [];
    for (var i = 0; i < this.dateNow; i++) {
      hours.push(i);
    }
    return hours;
  };

  getDisabledMinutes1 = (selectedHour: number) => {
    var minutes = [];
    if (selectedHour === moment().hour()) {
      for (var i = 0; i < this.minute; i++) {
        minutes.push(i);
      }

      return minutes;
    } else {
      var startHours = this.startTime.getHours();
      console.log(typeof this.startTime);

      if (selectedHour === startHours) {
        for (var i = 0; i < this.minute; i++) {
          minutes.push(i);
        }
        console.log('else');
      }

      return minutes;
    }
  };
  // ?-----------------------------------------------
  getDisabledHours = () => {
    var hours = [];
    // // // console.log(moment().hour(),'hr')
    for (var i = 0; i < moment().hour(); i++) {
      hours.push(i);
    }
    return hours;
  };

  getDisabledMinutes = (selectedHour: number) => {
    this.dateNow = selectedHour;
    var minutes = [];
    if (selectedHour === moment().hour()) {
      for (var i = 0; i < moment().minute(); i++) {
        minutes.push(i);
      }
    }
    return minutes;
  };

  changetime = (value: Date): void => {
    this.checkDate = value;
    console.log(value, 'value');
    if (value === null) {
      this.isDisabled = true;

      // alert('จำเป็นต้องระบุข้อมูล')
    } else {
      this.isDisabled = false;

      console.log(value.getHours(), 'valuess');

      this.minute = value.getMinutes();
    }
  };
  changetime1 = (value: Date): void => {
    if (value === null) {
      console.log('จำเป็นต้องเลือกต้องเลือก Start time ก่อน');
      this.isChecked = true;
    } else {
      console.log(value, 'value2');
      this.isChecked = false;
    }
  };
}
