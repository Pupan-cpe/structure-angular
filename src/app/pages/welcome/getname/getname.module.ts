import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GetnameRoutingModule } from './getname-routing.module';
import { GetnameComponent } from './getname.component';


@NgModule({
  declarations: [
    GetnameComponent
  ],
  imports: [
    CommonModule,
    GetnameRoutingModule
  ]
})
export class GetnameModule { }
