import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FruitsService } from 'src/app/shared/service/fruits.service';

@Component({
  selector: 'app-getname',
  templateUrl: './getname.component.html',
  styleUrls: ['./getname.component.scss'],
})
export class GetnameComponent implements OnInit {
  id: string;
  name: string;
  constructor(
    private activeRoute: ActivatedRoute,

    private apicall: FruitsService
  ) {
    this.id = this.activeRoute.snapshot.paramMap.get('id') as string;
    this.name = '';
  }
  ngOnChanges(): void {

    console.log('testssssssssssssssssssss');
  }
  ngOnInit(): void {
    console.log(this.id);
    this.apicall.getbyid(this.id).then((res) => {
      console.log(res);
      this.name = res.data.name;
    });
  }
}
