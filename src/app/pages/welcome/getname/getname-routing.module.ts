import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GetnameComponent } from './getname.component';

const routes: Routes = [{ path: ':id', component: GetnameComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GetnameRoutingModule { }
