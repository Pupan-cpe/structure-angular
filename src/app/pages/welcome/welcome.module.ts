import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { WelcomeRoutingModule } from './welcome-routing.module';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { WelcomeComponent } from './welcome.component';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { HighchartsChartComponent, HighchartsChartModule } from 'highcharts-angular';
// import { HighchartsChartComponent } from 'highcharts-angular';
@NgModule({
  imports: [WelcomeRoutingModule,SharedModule,NzSwitchModule,NzPopconfirmModule,HighchartsChartModule],
  declarations: [WelcomeComponent],
  exports: [WelcomeComponent]
})
export class WelcomeModule { }
