import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { firstValueFrom, tap } from 'rxjs';
import { ResultAPI } from '../model/result-api.model';

@Injectable({
  providedIn: 'root',
})
export class FruitsService {
  constructor(private http: HttpClient) {}

  // getFruits = (): Promise<any> =>{

  //   return this.http.get(`/api/v1/fruit`).toPromise();
  // }

  // getprofile = (): Promise<any> =>{
  //    return this.http.get(`/api/v1/train`).toPromise();
  //   }

  getprofile111 = async (): Promise<any> => {
    const get$ = this.http.get(`/api/v1/train`);
    const res2 = await firstValueFrom(get$).then((res) => {
      console.log(res);
    });
  };

  ddd = (): void => {
    const get$ = this.http
      .get(`/api/v1/train`)
      .pipe(tap(() => localStorage.clear()));
    const res2 = firstValueFrom(get$)
      .then(() => true)
      .catch(() => false);
    // async  .then(res => console.log(res));
  };

  // return this.http
  //       .post(this.uri('/v1/signout'), null)
  //       .pipe(
  //         tap(() => {
  //           localStorage.clear();
  //         })
  //       )
  //       .toPromise()
  //       .then(() => true)
  //       .catch(() => false);

  getbyid = (id: string): Promise<any> => {
    return this.http.get(`/api/v1/train/${id}`).toPromise();
  };
  getprofile = async (): Promise<ResultAPI> => {
    const get$ = this.http.get(`/api/v1/train`);
    const res2 = await firstValueFrom(get$);
    return res2;
  };
}
