import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResultAPI } from '../model/result-api.model';
@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  pixel: any;
  constructor(private http: HttpClient) {}

  getprofile = (): Promise<any> => {
    return this.http.get<ResultAPI>(`/api/v1/train`).toPromise();
  };

  sendPixel = (data:any):void => {
    this.pixel = data;

  }
}
