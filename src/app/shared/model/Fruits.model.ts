
//? ไม่รู้ประเภทของข้อมูล

export interface test1 {
  id: any;
  name: any;
  full_name: any;

}

//? รู้ประเภทของข้อมูล

export interface Test2 {
  id: number;
  name: string;
  full_name: string;

}

export interface Profile {
  id?: number;
  name?: string;
  full_name?: string;
}


