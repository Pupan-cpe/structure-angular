import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './core/layout/layout.component';


const routes: Routes = [

  {
    path: '',
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
  {
    path: 'welcome',
    component: LayoutComponent,
    loadChildren: () =>
      import('./pages/welcome/welcome.module').then((m) => m.WelcomeModule),
  },

  {
    path: 'getname',
    component: LayoutComponent,
    loadChildren: () =>
      import('./pages/welcome/getname/getname.module').then((m) => m.GetnameModule),
  },
  {
    path: '**',
    redirectTo: '/error',
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
