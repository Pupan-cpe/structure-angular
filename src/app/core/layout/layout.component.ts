import { Component, HostListener, OnInit } from '@angular/core';
import {ProfileService} from '../../shared/service/profile.service'
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  windowInnerWidth: any;
  menuCollapsed: boolean;
  subMenuOpened: boolean;
  subMenuSelected: boolean;
  isCollapsed = false;


  constructor(private sendData: ProfileService) {

    this.menuCollapsed = true;
    this.subMenuOpened = false;
    this.subMenuSelected = false;
    
   }

   ngOnInit(): void {
    this.windowInnerWidth = window.innerWidth;

    if (this.windowInnerWidth < 576) {
      this.menuCollapsed = true;
      console.log('mobile');

    } else {
      this.menuCollapsed = false;
      console.log('pc');


    }
    this.sendPixel();

    this.subMenuOpened = false;
  }
  @HostListener('window:resize', ['$event'])
  onResize = (): void => {
    this.windowInnerWidth = window.innerWidth;
    if (this.windowInnerWidth < 576) {
      this.menuCollapsed = true;
      console.log('mobile');

    }
  };

  sendPixel = (): void => {
    this.sendData.sendPixel(this.menuCollapsed);
    
  }
  forceDeselectSubMenu = (): void => {
    this.subMenuSelected = true;
    setTimeout(() => {
      this.subMenuSelected = false;
    }, 0);
  };
}
